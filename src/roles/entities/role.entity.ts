import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({
  versionKey: false,
  timestamps: true,
})
export class Role extends Document {
  @Prop({
    unique: true,
    required: true,
  })
  name: string;
}

const RoleSchema = SchemaFactory.createForClass(Role);

export { RoleSchema };
