import { Injectable } from '@nestjs/common';
import { BadRequestException } from '@nestjs/common/exceptions';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { compareHash } from 'src/common/utils/password-hash';
import { Subscription } from 'src/subscription/entities/subscription.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

type userPayload = {
  username?: string;
  email?: string;
  password: string;
};

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private userModel: Model<User>,
    @InjectModel(Subscription.name)
    private subscriptionModel: Model<Subscription>,
  ) {}

  create(createUserDto: CreateUserDto) {
    const entity = this.userModel.create(createUserDto);
    return entity;
  }

  findAll() {
    return `This action returns all user`;
  }

  async findAuthenticatedUser(user: userPayload) {
    const { username, email, password } = user;
    const userData = await this.userModel
      .findOne({
        $or: [{ username }, { email }],
      })
      .populate('role');

    const validatePassword = await compareHash(userData.password, password);
    if (!validatePassword) throw new BadRequestException('Wrong Password');
    return userData;
  }

  async getSubscriptions(user: string) {
    const subscriptions = await this.subscriptionModel.find({
      user: user,
      active: true,
    });
    return subscriptions;
  }

  findOne(id: number) {
    return `This action returns a #${id} user`;
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
