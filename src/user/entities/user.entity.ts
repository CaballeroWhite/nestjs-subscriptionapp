import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

import mongoose, { Document } from 'mongoose';

import { generateSalt, generateHash } from 'src/common/utils/password-hash';

@Schema({
  versionKey: false,
  timestamps: true,
})
export class User extends Document {
  @Prop({
    index: true,
    unique: true,
    required: true,
  })
  username: string;

  @Prop({
    unique: true,
    required: true,
  })
  email: string;

  @Prop({
    required: true,
  })
  password: string;

  @Prop({
    type: Boolean,
    default: true,
  })
  active: boolean;

  @Prop({
    type: Boolean,
    default: false,
  })
  confirmedAccount: boolean;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Role',
  })
  role: string;
}

const UserSchema = SchemaFactory.createForClass(User);

UserSchema.pre('save', function (this: User, next: any) {
  const rounds = generateSalt();
  this.password = generateHash(this.password, Number(rounds));
  next();
});

export { UserSchema };
