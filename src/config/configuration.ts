export default () => ({
  database: {
    uri: process.env.MONGO_URI,
  },
  jwt: {
    jwt_secret: process.env.JWT_SECRET,
  },
});
