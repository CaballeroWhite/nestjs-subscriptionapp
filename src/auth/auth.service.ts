import { Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/user/user.service';

import { CreateAuthDto } from './dto/create-auth.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';

type userPayload = {
  username?: string;
  email?: string;
  password: string;
};

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async findUserAuth(user: userPayload) {
    const checkIfUserExist = await this.userService.findAuthenticatedUser(user);
    return checkIfUserExist;
  }

  async authentication(data: userPayload) {
    const checkUser = await this.findUserAuth(data);

    if (checkUser)
      return {
        token: this.jwtService.sign({
          username: checkUser.username,
          role: checkUser.role,
          sub: checkUser._id,
        }),
      };
    throw new NotFoundException();
  }

  create(createAuthDto: CreateAuthDto) {
    return 'This action adds a new auth';
  }

  findAll() {
    return `This action returns all auth`;
  }

  findOne(id: number) {
    return `This action returns a #${id} auth`;
  }

  update(id: number, updateAuthDto: UpdateAuthDto) {
    return `This action updates a #${id} auth`;
  }

  remove(id: number) {
    return `This action removes a #${id} auth`;
  }
}
