import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsDate,
  IsOptional,
  IsString,
  MinLength,
} from 'class-validator';

export class CreateSubscriptionDto {
  @IsString()
  @MinLength(3)
  name: string;

  @IsDate()
  @Type(() => Date)
  adquiredDate: Date;

  @IsDate()
  @Type(() => Date)
  expirationDate: Date;

  @IsOptional()
  @IsBoolean()
  active?: boolean;

  @IsOptional()
  @IsString()
  logo?: string;

  @IsString()
  @IsOptional()
  user: string;
}
