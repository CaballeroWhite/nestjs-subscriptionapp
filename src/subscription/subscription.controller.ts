import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
  Request,
} from '@nestjs/common';
import { SubscriptionService } from './subscription.service';
import { CreateSubscriptionDto } from './dto/create-subscription.dto';
import { UpdateSubscriptionDto } from './dto/update-subscription.dto';
import { ParseMongoIdPipe } from 'src/common/pipes/parse-mongo-id/parse-mongo-id.pipe';

import { JwtAuthGuard } from 'src/common/Guards/jwt-auth.guard';

import {
  ISubscriptionFilterFields,
  SubscriptionFilter,
} from 'src/common/Filters/Subscription.Filter';
import { OwnSubscriptionGuard } from 'src/common/Guards/own-subscription.guard';
import { Roles } from 'src/common/Decorators/roles-decorator';
import { Role } from 'src/common/interfaces/roles.enum';
type FiltersQueryD = {
  fields: ISubscriptionFilterFields;
  filters: ISubscriptionFilterFields;
  populate: string[];
};
@Controller('subscription')
@UseGuards(JwtAuthGuard)
export class SubscriptionController {
  constructor(private readonly subscriptionService: SubscriptionService) {}

  @Post()
  @Roles(Role.Subscriber, Role.Admin)
  create(
    @Body() createSubscriptionDto: CreateSubscriptionDto,
    @Request() request,
  ) {
    const { userId } = request.user;
    createSubscriptionDto.user = userId;

    return this.subscriptionService.create(createSubscriptionDto);
  }

  @Get()
  @Roles(Role.Admin)
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  findAll(@Query() filters: FiltersQueryD, @Request() request) {
    return this.subscriptionService.findAll(
      new SubscriptionFilter(filters.filters).filters,
    );
  }

  @Get(':id')
  @Roles(Role.Subscriber)
  @UseGuards(OwnSubscriptionGuard)
  findOne(
    @Param('id', ParseMongoIdPipe) id: string,
    @Query() filter: FiltersQueryD,
  ) {
    const fields = Object.keys(filter.fields).toString().replaceAll(',', ' ');
    return this.subscriptionService.findOne(id, fields);
  }

  @Patch(':id')
  @Roles(Role.Subscriber)
  @UseGuards(OwnSubscriptionGuard)
  update(
    @Param('id', ParseMongoIdPipe) id: string,
    @Body() updateSubscriptionDto: UpdateSubscriptionDto,
  ) {
    return this.subscriptionService.update(id, updateSubscriptionDto);
  }

  @Roles(Role.Subscriber, Role.Admin)
  @UseGuards(OwnSubscriptionGuard)
  @Patch('disable/:id')
  disableSubscription(@Param('id', ParseMongoIdPipe) id: string) {
    return this.subscriptionService.disableSubscription(id);
  }

  @Roles(Role.Subscriber, Role.Admin)
  @UseGuards(OwnSubscriptionGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.subscriptionService.remove(+id);
  }
}
