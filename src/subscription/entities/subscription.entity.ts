import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

//export type PokemonDocument = HydratedDocument<Subscription>;

@Schema({
  versionKey: false,
  timestamps: true,
})
export class Subscription extends Document {
  @Prop({
    index: true,
    required: true,
  })
  name: string;

  @Prop({
    type: Date,
    required: true,
  })
  adquiredDate: Date;

  @Prop({
    type: Date,
    required: true,
  })
  expirationDate: Date;

  @Prop({
    type: Boolean,
    default: true,
  })
  active: boolean;

  @Prop({
    required: false,
    type: String,
  })
  logo: string;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  })
  user: string;
}

export const SubscriptionSchema = SchemaFactory.createForClass(Subscription);
