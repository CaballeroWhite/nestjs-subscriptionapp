import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model } from 'mongoose';
import { CreateSubscriptionDto } from './dto/create-subscription.dto';
import { UpdateSubscriptionDto } from './dto/update-subscription.dto';
import { Subscription } from './entities/subscription.entity';

@Injectable()
export class SubscriptionService {
  constructor(
    @InjectModel(Subscription.name)
    private subscriptionModel: Model<Subscription>,
  ) {}
  create(createSubscriptionDto: CreateSubscriptionDto) {
    return this.subscriptionModel.create(createSubscriptionDto);
  }

  findAll(filters: FilterQuery<Subscription>) {
    return this.subscriptionModel.find(filters).populate('user');
  }

  findOne(id: string, fields: string) {
    return this.subscriptionModel
      .findOne({ _id: id })
      .populate('user')
      .select(fields);
  }

  update(id: string, updateSubscriptionDto: UpdateSubscriptionDto) {
    return this.subscriptionModel.findOneAndUpdate(
      { _id: id },
      updateSubscriptionDto,
      { returnDocument: 'after' },
    );
  }

  disableSubscription(id: string) {
    return this.subscriptionModel.findByIdAndUpdate(
      id,
      { active: false },
      {
        new: true,
      },
    );
  }

  async checkOwner(user: string, subscription: string) {
    const findSubscription = await this.subscriptionModel.findOne({
      _id: subscription,
      user,
    });
    return findSubscription;
  }

  remove(id: number) {
    return `This action removes a #${id} subscription`;
  }
}
