import * as bcrypt from 'bcrypt';

function generateSalt(rounds = 10) {
  return bcrypt.genSaltSync(rounds);
}

export const generateHash = (password: string, salt: number) => {
  return bcrypt.hashSync(password, salt);
};

export const compareHash = (
  hassPwd: string,
  plainPwd: string,
): Promise<boolean> => {
  return bcrypt.compare(plainPwd, hassPwd);
};

export { generateSalt };
