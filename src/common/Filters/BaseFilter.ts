import { FilterQuery } from 'mongoose';

export interface BaseFilter<T> {
  filters: FilterQuery<T>;
}
