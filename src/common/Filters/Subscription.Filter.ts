import mongoose, { FilterQuery } from 'mongoose';
import { Subscription } from 'src/subscription/entities/subscription.entity';
import { BaseFilter } from './BaseFilter';

export interface ISubscriptionFilterFields {
  _id?: string;
  user?: string;
  name?: string;
  active?: string;
}

export class SubscriptionFilter implements BaseFilter<Subscription> {
  filters: FilterQuery<Subscription> = {};

  constructor(fieldsToFilter: ISubscriptionFilterFields) {
    if (fieldsToFilter) {
      if (fieldsToFilter.user) this.filters.user = fieldsToFilter.user;
      if (fieldsToFilter._id)
        this.filters._id = new mongoose.mongo.ObjectId(fieldsToFilter._id);
      this.filters.active = fieldsToFilter.active ?? true;
    }
  }
}
