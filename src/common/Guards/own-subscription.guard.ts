import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Inject,
  Injectable,
} from '@nestjs/common';
import { SubscriptionService } from 'src/subscription/subscription.service';

@Injectable()
export class OwnSubscriptionGuard implements CanActivate {
  constructor(
    @Inject(SubscriptionService)
    private readonly subscriptionService: SubscriptionService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    const subscriptionId = request.params.id;

    const check = await this.subscriptionService.checkOwner(
      user.userId,
      subscriptionId,
    );

    if (!check)
      throw new ForbiddenException('Hey, step back, you are not the owner');
    return true;
  }
}
