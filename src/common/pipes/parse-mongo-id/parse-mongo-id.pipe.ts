import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { isValidObjectId } from 'mongoose';
import InvalidMongoIdException from 'src/common/Exceptions/InvalidMongoId.Exception';

@Injectable()
export class ParseMongoIdPipe implements PipeTransform {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  transform(value: any, _metadata: ArgumentMetadata) {
    if (!isValidObjectId(value)) throw new InvalidMongoIdException();
    return value;
  }
}
