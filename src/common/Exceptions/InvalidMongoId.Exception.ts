import { HttpException, HttpStatus } from '@nestjs/common';

export default class InvalidMongoIdException extends HttpException {
  constructor(
    message = 'Invalid Mongo Id',
    httpStatus: HttpStatus = HttpStatus.BAD_REQUEST,
  ) {
    super(message, httpStatus);
  }
}
